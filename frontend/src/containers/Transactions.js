import React, { Component } from "react";
import { PageHeader, Table } from "react-bootstrap";
import Modal from './AddTransaction';

class Transactions extends Component {
    static contextTypes = {
        router: () => true, // replace with PropTypes.object if you use them
      }
    constructor(props) {
        super(props);

        this.state = {
          transactions: [],
          amount:0,
          type:'',
          show:false,
          errors:{},
          accId:0
        };
        
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.handleAddTransaction = this.handleAddTransaction.bind(this);
        this.handleChange = this.handleChange.bind(this);    
        this.validateForm = this.validateForm.bind(this);
    }

    showModal = () => {
        this.setState({ show: true });
      };
    
      hideModal = () => {
        //   if(this.validateForm()){
            this.setState({ show: false });
        //   }        
      };
      handleChange(event) {
        this.setState({[event.target.id] : event.target.value});
    }

    componentDidMount() {
        this.setState({accId:this.props.location.state.accId});
        fetch(`http://localhost:8080/transactions?accountId=${this.props.location.state.accId}`)
        .then(res => res.json())
        .then(transactions => (this.setState({transactions})))
        .catch(err => console.log(err))
    }


    componentDidUpdate(){
        fetch(`http://localhost:8080/transactions?accountId=${this.state.accId}`)
        .then(res => res.json())
        .then(transactions => (this.setState({transactions})))
        .catch(err => console.log(err))
    }

    validateForm(){
        let errors = {}
        let isValid = true;
        if (typeof this.state.amount !== "undefined") {
                
            var pattern = new RegExp(/^[0-9]+$/);
            if (!pattern.test(this.state.amount)) {
              isValid = false;
              errors["amount"] = "Please enter only numbers.";
            }
          }

          if(!(this.state.type.length)>0 && (this.state.type != 'credit' || this.state.type != 'debit')){
            isValid = false;
            errors["type"] = "Please enter type of transaction as credit or debit.";
          }          

          this.setState({
            errors: errors
          });
      
          return isValid;
    }

    handleAddTransaction() {
        if(this.validateForm()){   
            this.setState({ show: false });         
        fetch(`http://localhost:8080/transactions`,{
             // Adding method type
    method: "POST",
      
    // Adding body or contents to send
    body: JSON.stringify({
        amount: `${this.state.amount}`,
        type:`${this.state.type}`,
        accId:`${this.props.location.state.accId}`
    }),
      
    // Adding headers to the request
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    }
        }).then(response => {
            if (response.status === 200) {
                this.props.authenticate(true);
                alert("Transaction added Successfully");
                this.props.history.push('/transactions', {'user': this.props.location.state.accId});                
            } else {
                alert("Transaction Failed");
            }
        })
       .catch(err => this.setState({ requestFailed: true }))
    }
    }

    render() {
        return (
            <div className="Transaction">
                <PageHeader>Your Transactions</PageHeader>
                <Table>
                    <thead>
                        <tr>
                            <th>AccountId</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>TransactionType</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.transactions.map(tran => <tr>
                            <th>{tran.accountId}</th>
                            <th>{tran.date}</th>
                            <th>{tran.amount}</th>
                            <th>{tran.transactionType}</th>
                            <th>{tran.status}</th>
                        </tr>)}
                    </tbody>
                </Table>
                <main>
                <Modal show={this.state.show} handleClose={this.hideModal} amount={this.state.amount} type={this.state.type} handleAddTransaction={this.handleAddTransaction} handleChange={this.handleChange} msg={this.state.errors.amount} typemsg={this.state.errors.type}>                            
          </Modal>
                  <button type="button" onClick={this.showModal}> Add Transaction</button>
                  </main>
                <button
        className="button icon-left"
        onClick={this.context.router.history.goBack}>
          Back
      </button>
            </div>
        );
    }
}

export default Transactions;
