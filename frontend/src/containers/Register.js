import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Register.css";

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            cpassword: '',
            errors:{}
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    validateForm() {
        let errors = {};
        let isValid = true;
    
        if (!(this.state.first_name.length > 0)) {
          isValid = false;
          errors["first_name"] = "Please enter your first name.";
        }
    
        if (!(this.state.last_name.length > 0)) {
          isValid = false;
          errors["last_name"] = "Please enter your last name";
        }
    
        if (!(this.state.email.length > 0)) {
            isValid = false;
            errors["email"] = "Please enter your email";
        }

        if (!(this.state.password.length > 0)) {
            isValid = false;
            errors["password"] = "Please enter password";
        }

        if (!(this.state.cpassword.length > 0)) {
            isValid = false;
            errors["cpassword"] = "Please enter confirm password";
        }

        if(!(this.state.password === this.state.cpassword) && this.state.cpassword.length > 0){
            isValid = false;
            errors["cpassword"] = "Confirm password should match with password";
        }
    
        this.setState({
          errors: errors
        });
    
        return isValid;
    }

    handleChange(event) {
        this.setState({[event.target.id] : event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        if(this.validateForm()){
        fetch(`http://localhost:8080/user/register`,{
             // Adding method type
    method: "POST",
      
    // Adding body or contents to send
    body: JSON.stringify({
        first_name:`${this.state.first_name}`,
        last_name:`${this.state.last_name}`,
        email:`${this.state.email}`,
        password:`${this.state.password}`
    }),
      
    // Adding headers to the request
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    }
        }).then(response => {
            if (response.status === 200) {
                alert("User "+ this.state.email+ " is succeffully registered");
                this.props.history.push('/');
            }
            else{
                alert("User "+ this.state.email+ " registration failed");
            }
        })
       .catch(err => this.setState({ requestFailed: true }))
    }
    }
    render() {
        return (
            <div className="Register">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="first_name" bsSize="large">
                        <ControlLabel>First Name</ControlLabel>
                        <FormControl
                            autoFocus
                            type="first_name"
                            placeholder="Enter the First Name"
                            required='true'
                            value={this.state.first_name}
                            onChange={this.handleChange}
                         />
                    </FormGroup>
                    <div className="text-danger">{this.state.errors.first_name}</div>
                    <FormGroup controlId="last_name" bsSize="large">
                        <ControlLabel>Last Name</ControlLabel>
                        <FormControl
                            type="last_name"
                            placeholder="Enter the Last Name"
                            value={this.state.last_name}
                            onChange={this.handleChange}
                    />
                    </FormGroup>
                    <div className="text-danger">{this.state.errors.last_name}</div>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            type="email"
                            placeholder="Enter the Email Id"
                            value={this.state.email}
                            onChange={this.handleChange}
                    />
                    </FormGroup>
                    <div className="text-danger">{this.state.errors.email}</div>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            type="password"
                            placeholder="Enter the password"
                            value={this.state.password}
                            onChange={this.handleChange}
                    />
                    </FormGroup>
                    <div className="text-danger">{this.state.errors.password}</div>
                    <FormGroup controlId="cpassword" bsSize="large">
                        <ControlLabel>Confirm Password</ControlLabel>
                        <FormControl
                            type="password"
                            placeholder="Enter the confirm password"                            
                            value={this.state.cpassword}
                            onChange={this.handleChange}
                    />
                    </FormGroup>
                    <div className="text-danger">{this.state.errors.cpassword}</div>
                    <Button id="submit" block bsSize="large" type="submit">
                        Register
                    </Button>
                </form>
            </div>
        );
    }
}
export default Register;
