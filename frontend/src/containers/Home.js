import React, { Component } from "react";
import "./Home.css";
import bankpng from '../bank.png'

class Home extends Component {
    
    render() {
        return (
            <div className="Home">                
            <h2>Welcome to the banking Application</h2>   
            <img src={bankpng} alt="cur" class="center"/>              
            </div>
        );
    }  
}

export default Home;
