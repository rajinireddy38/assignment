import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Overview.css";
import _ from 'lodash';

class Overview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
        this.handleAccount = this.handleAccount.bind(this);
        this.handleProfile = this.handleProfile.bind(this);
    }

    handleAccount(){
        this.props.history.push('/accounts', {'user': this.props.location.state.user});
    }

    handleProfile(){
        this.props.history.push('/profile', {'user': this.props.location.state.user});
    }

    render() {
        return (            
            <div className="Overview">
                <h2>Welcome to the {this.props.location.state.user}</h2>                                
                                 <div>
                                <button onClick={this.handleAccount}> Account</button>
                                <button onClick={this.handleProfile}> Profile</button>                                
                                </div>                                           
            </div>
        );
    }  
}
export default Overview;
