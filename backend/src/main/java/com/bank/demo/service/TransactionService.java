package com.bank.demo.service;

import java.util.List;
import java.util.Optional;

import com.bank.demo.dao.AccountRepository;
import com.bank.demo.dao.TransactionRepository;
import com.bank.demo.entities.Account;
import com.bank.demo.entities.Transactions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    public TransactionService(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    public Transactions createTransactions(long amount,String type, long accountId) {
        Account account = accountRepository.findByAccountId(accountId);          
        if (account != null && amount>0) {              
            if(type.equalsIgnoreCase("credit")){
                account.setBalance(account.getBalance()+amount);
            }
            else{
                account.setBalance(account.getBalance()-amount);
            }
            accountRepository.save(account);
            Transactions transactions = new Transactions(accountId, amount, type, "success");
            return transactionRepository.save(transactions);            
        }
        return null;
    }

    public List<Transactions> getTransactionsByAccountId(long accId) {
        return transactionRepository.findByAccountId(accId);
    }
}
