import './AddAccount.css';
import React from 'react';
import {  FormGroup, ControlLabel,FormControl, Button, PageHeader, Table } from "react-bootstrap";

const Modal = ({ handleClose, show, children, credit, handleAddAccount, handleChange, msg }) => {
const showHideClassName = show ? "modal display-block" : "modal display-none";

  return (
        <div className={showHideClassName} class="form-popup">
        <section className="modal-main">
          {children}
          <form class="form-container">
          <h2>Add Account</h2>
          <FormGroup controlId="credit" bsSize="large">
                        <ControlLabel>Initial Deposit Amount</ControlLabel>
                        <FormControl
                            autoFocus
                            type="credit"
                            value={credit}
                            onChange={handleChange}
                    />
                    </FormGroup>                    
                    <div className="text-danger">{msg}</div>
            <button type="button" onClick={() => { handleAddAccount();}} class="form-container btn">Submit</button>
            
            <button type="button" onClick={handleClose} class="form-container cancel">Close</button>
          </form>
        </section>
        </div>
      );
};

export default Modal;