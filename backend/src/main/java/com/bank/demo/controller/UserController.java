package com.bank.demo.controller;

import java.util.List;
import java.util.Map;

import com.bank.demo.entities.User;
import com.bank.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/user")
    public ResponseEntity<User> getUser(@RequestParam String emailAddress) {
        return new ResponseEntity<User>(userService.getUserByEmail(emailAddress), HttpStatus.OK);
    }

    @PostMapping("/user/sign-in")
    public ResponseEntity<User> userSignin(@RequestBody Map<String, String> bodyParams) {
        User user = userService.getUserByEmail(bodyParams.get("email").toString());
        if (user.getEmailAddress().isEmpty()) {
            return new ResponseEntity<User>(user, HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getUser() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping("/user/register")
    public ResponseEntity<String> createUser(@RequestBody Map<String, String> bodyParams) {
        long userId = userService.newUser(bodyParams.get("first_name").toString(),
                bodyParams.get("last_name").toString(), bodyParams.get("email").toString(),
                bodyParams.get("password").toString());
        if (userId != 0 ) {
            return new ResponseEntity<String>("User " + userId + " created successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("User " + userId + " creation failed", HttpStatus.NOT_MODIFIED);
        }
    }

}
