package com.bank.demo.controller;

import java.util.List;
import java.util.Map;

import com.bank.demo.entities.Account;
import com.bank.demo.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@CrossOrigin
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/account")
    public ResponseEntity<String> addAccount(@RequestBody Map<String, String> bodyParams) {
        long initialCredit = Long.parseLong(bodyParams.get("initialCredit").toString());
        String emailAddress = bodyParams.get("emailAddress").toString();
        long accountId = this.accountService.newAccount(initialCredit, emailAddress);
        return new ResponseEntity<String>("Account " + accountId + " for the user " + emailAddress + " created successfully",
                HttpStatus.OK);
    }

    @GetMapping("/account")
    public ResponseEntity<Account> getAccount(@RequestParam Long id) {
        return ResponseEntity.ok(accountService.getAccountbyId(id));
    }

    @GetMapping("/accounts/id")
    public ResponseEntity<List<Account>> getAccountsByCustID(@RequestParam long id) {
        return ResponseEntity.ok(accountService.getAccountsbyCustId(id));
    }

    @GetMapping("/accounts")
    public ResponseEntity<List<Account>> getAccountsByCustEmailAddress(@RequestParam String emailAddress) {
        return ResponseEntity.ok(accountService.getAccountsbyEmailId(emailAddress));
    }
}
