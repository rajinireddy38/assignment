package com.bank.demo.service;

import java.util.List;
import java.util.Optional;

import com.bank.demo.dao.AccountRepository;
import com.bank.demo.dao.TransactionRepository;
import com.bank.demo.dao.UserRepository;
import com.bank.demo.entities.Account;
import com.bank.demo.entities.Transactions;
import com.bank.demo.entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public AccountService(AccountRepository accountRepository, UserRepository userRepository,
            TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.transactionRepository = transactionRepository;
    }

    public Long newAccount(long initialCredit, String emailAddress) {
        User user = userRepository.findByEmailAddress(emailAddress);
        long accountId = 0;
        if (user!= null) {
            Account account = new Account(initialCredit, user.getId());
            accountId = accountRepository.save(account).getAccountId();
            if(initialCredit>0){
                Transactions transactions = new Transactions(accountId, initialCredit, "credit", "success");
                transactionRepository.save(transactions);
            }            
        }
        return accountId;
    }

    public void deleteAccount(Long id) {
        accountRepository.deleteById(id);
    }

    public Account getAccountbyId(Long id) {
        return accountRepository.findById(id).orElseThrow();
    }

    public List<Account> getAccountsbyCustId(Long custId) {
        return accountRepository.findByCustomerId(custId);
    }

    public List<Account> getAccountsbyEmailId(String emailAddress) {
        User user = userRepository.findByEmailAddress(emailAddress);
        return getAccountsbyCustId(user.getId());
    }
}
