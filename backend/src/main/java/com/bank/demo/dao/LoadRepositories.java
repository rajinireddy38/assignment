package com.bank.demo.dao;

import com.bank.demo.entities.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

public class LoadRepositories {
    private static final Logger log = LoggerFactory.getLogger(LoadRepositories.class);

    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository) {
        return args -> {
            log.info("Preloading" + new User("Rajini", "Nanja Reddy", "rajinireddy38@gmail.com", "abc@123"));
        };
    }
}
