import './AddTransaction.css';
import React from 'react';
import {  FormGroup, ControlLabel,FormControl, Button, PageHeader, Table } from "react-bootstrap";

const Modal = ({ handleClose, show, children, amount, type, handleAddTransaction, handleChange, msg, typemsg }) => {
const showHideClassName = show ? "modal display-block" : "modal display-none";

  return (
        <div className={showHideClassName} class="form-popup">
        <section className="modal-main">
          {children}
          <form class="form-container">
          <h2>Add Transaction</h2>
          <FormGroup controlId="amount" bsSize="large">
                        <ControlLabel>Deposit/Withdraw Amount</ControlLabel>
                        <FormControl
                            autoFocus
                            type="amount"
                            placeholder="Enter the amount"
                            value={amount}                            
                            onChange={handleChange}
                    />
                    </FormGroup>                    
                    <div className="text-danger">{msg}</div>                                         
                    <FormGroup controlId="type" bsSize="large">                        
                    <ControlLabel>Type</ControlLabel>
                        <FormControl                            
                            type="type"
                            placeholder="Enter credit or debit"
                            value={type}
                            onChange={handleChange}
                    />
                    </FormGroup>                    
                    <div className="text-danger">{typemsg}</div>
            <button type="button" onClick={() => { handleAddTransaction();}} class="form-container btn">Submit</button>
            
            <button type="button" onClick={handleClose} class="form-container cancel">Close</button>
          </form>
        </section>
        </div>
      );
};

export default Modal;