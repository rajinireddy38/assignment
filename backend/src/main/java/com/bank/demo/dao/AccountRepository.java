package com.bank.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.bank.demo.entities.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    public List<Account> findByCustomerId(Long customerId);
    public Account findByAccountId(long accountId);
}
