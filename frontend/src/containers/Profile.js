import { Component } from 'react';
import React from 'react'
import {  FormGroup, ControlLabel,FormControl, Button, PageHeader, Table } from "react-bootstrap";
import "./Profile.css";

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userJson:{}           
        };

        this.handleSubmit = this.handleSubmit.bind(this);  
      }

    componentDidMount() {
        fetch(`http://localhost:8080/user?emailAddress=${this.props.location.state.user}`)
        .then(res => res.json())
        .then(userJson => (this.setState({userJson})))
        .catch(err => console.log(err))
    }

    handleSubmit(){
        this.props.history.push('/overview', {'user': this.props.location.state.user});
    }


      render() {
        return (
            <div className="Profile">
                <h1>Profile Information</h1>                
                   <label>Name:</label>
                   <p>{this.state.userJson.name} {this.state.userJson.surname}</p>
                   <label>EmailAddress:</label>
                   <p>{this.state.userJson.emailAddress}</p>                   
                    <button
        onClick={this.handleSubmit}>
          Back
      </button>                                                                                                                                
            </div>
        );
      }
}

export default Profile;
