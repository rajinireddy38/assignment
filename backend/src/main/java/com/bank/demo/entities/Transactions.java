package com.bank.demo.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.boot.context.properties.bind.DefaultValue;

@Entity
public class Transactions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long accountId;
    private String transactionType;
    private long amount;
    private String status;
    private Date date;

    public Transactions(long accountId, long amount, @DefaultValue("IDEAL") String transactionType, String status) {
        this.accountId = accountId;
        this.amount = amount;
        this.transactionType = transactionType;
        this.status = status;
        this.date = new Date();
    }

    public Transactions() {

    }

    @Override
    public String toString() {
        return "Transactions [Status=" + status + ", amount=" + amount + ", date=" + date + ", id=" + id
                + ", transactionType=" + transactionType + "]";
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
