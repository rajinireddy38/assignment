package com.bank.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.demo.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public User findByEmailAddress(String emailAddress);
}
