import { Component, useState } from 'react';
import React from 'react'
import {  FormGroup, ControlLabel,FormControl, Button, PageHeader, Table } from "react-bootstrap";
import Modal from './AddAcount.js';

class Accounts extends Component {
    constructor(props) {
        super(props);

        this.state = {
          accounts: [],
          accountId: '',
          credit:0,
          show:false,
          errors:{}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddAccount = this.handleAddAccount.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleBack = this.handleBack.bind(this);
      }
      
      showModal = () => {
        this.setState({ show: true });
      };
    
      hideModal = () => {        
            this.setState({ show: false });        
      };

      handleChange(event) {
        this.setState({[event.target.id] : event.target.value});
    }

    componentDidMount() {
        fetch(`http://localhost:8080/accounts?emailAddress=${this.props.location.state.user}`)
        .then(res => res.json())
        .then(accounts => (this.setState({accounts})))
        .catch(err => console.log(err))
    }

    componentDidUpdate(){
        fetch(`http://localhost:8080/accounts?emailAddress=${this.props.location.state.user}`)
        .then(res => res.json())
        .then(accounts => (this.setState({accounts})))
        .catch(err => console.log(err))
    }
    
    handleSubmit(accountId){
        this.props.history.push({pathname:'/transactions', state: { accId: accountId }},{'user': this.props.location.state.user});
    }

    handleBack(){
        this.props.history.push('/overview', {'user': this.props.location.state.user});
    }

    validateForm(){
        let errors = {}
        let isValid = true;
        if (typeof this.state.credit !== "undefined") {
                
            var pattern = new RegExp(/^[0-9]+$/);
            if (!pattern.test(this.state.credit)) {
              isValid = false;
              errors["credit"] = "Please enter only numbers.";
            }
          }
          this.setState({
            errors: errors
          });
      
          return isValid;
    }

    handleAddAccount() {
        if(this.validateForm()){
            this.setState({ show: false });
        fetch(`http://localhost:8080/account`,{
             // Adding method type
    method: "POST",
      
    // Adding body or contents to send
    body: JSON.stringify({
        initialCredit: `${this.state.credit}`,
        emailAddress:`${this.props.location.state.user}`
    }),
      
    // Adding headers to the request
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    }
        }).then(response => {
            if (response.status === 200) {
                this.props.authenticate(true);
                alert("Account Created Successfully");
                this.props.history.push('/accounts', {'user': this.props.location.state.user});                
            } else {
                alert("Account Creation Failed");
            }
        })
       .catch(err => this.setState({ requestFailed: true }))
    }
    }
    render() {
        return (
            <div className="Account">
                <PageHeader>Your accounts</PageHeader>
                <Table>
                    <thead>
                        <tr>
                            <th>Account ID</th>
                            <th>Balance</th>
                            <th>Customer ID</th>
                            <th>IFSC Code</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.accounts.map(acc => {
                            return (
                                <tr key={acc.accountId} data-item={acc.accountId}>
                            <td>{acc.accountId}</td>
                            <td>{acc.balance}</td>
                            <td>{acc.customerId}</td>
                            <td>{acc.ifscCode}</td>
                            <button onClick={()=>this.handleSubmit(acc.accountId)}> View Transactions</button>
                            </tr>
                            )
                        })}                    
                    </tbody>                    
                </Table>
                {/* <button type="button" onClick={this.showModal}> Add Account</button> */}
            <main>
            <Modal show={this.state.show} handleClose={this.hideModal} credit={this.state.credit} handleAddAccount={this.handleAddAccount} handleChange={this.handleChange} msg={this.state.errors.credit}>            
          </Modal>
                  <button type="button" onClick={this.showModal}> Add Account</button>
                  </main>
                  <button    
        onClick={this.handleBack}>
          Back
      </button>  
            </div>
        );
    }
}

export default Accounts;
