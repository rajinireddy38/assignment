package com.bank.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
@TableGenerator(name="tab", initialValue=16000)
public class Account {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="tab")
    private long accountId;
    private String ifscCode;
    private long customerId;
    private long balance;

    public Account(long credit, long custId) {
        this.balance = this.balance + credit;
        this.customerId = custId;
        this.ifscCode = "NL0";
    }

    public Account() {
    };

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

}
