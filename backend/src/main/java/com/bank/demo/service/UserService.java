package com.bank.demo.service;

import java.util.List;
import com.bank.demo.dao.UserRepository;
import com.bank.demo.entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> all() {
        return userRepository.findAll();
    }

    public Long newUser(String name, String surname, String emailAddress, String password) {
        User user = new User(name, surname, emailAddress, password);
        user = userRepository.save(user);
        return user.getId();
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserByEmail(String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress);
    }

    public User getUser(long id) {
        return userRepository.findById(id).orElseThrow();
    }

    public Long updatUser(String name, String surname, Long id) {
        return userRepository.findById(id).map(user -> {
            user.setName(name);
            user.setSurname(surname);
            user = userRepository.save(user);
            return user.getId();
        }).orElseGet(() -> {
            User user = new User();
            user.setName(name);
            user.setSurname(surname);
            user.setId(id);
            user = userRepository.save(user);
            return user.getId();
        });
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}
