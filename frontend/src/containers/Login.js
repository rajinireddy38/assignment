import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import _ from 'lodash';


class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: '',
            password: '',
            errors:{}
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    validateForm() {
            let errors = {};
            let isValid = true;
        
            if (!(this.state.user.length > 0)) {
              isValid = false;
              errors["user"] = "Please enter username.";
            }
        
            if (!(this.state.password.length > 0)) {
              isValid = false;
              errors["password"] = "Please enter your password";
            }
        
            if (typeof this.state.email !== "undefined") {
                
              var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
              if (!pattern.test(this.state.email)) {
                isValid = false;
                errors["user"] = "Please enter valid email address.";
              }
            }
        
            this.setState({
              errors: errors
            });
        
            return isValid;
    }

    handleChange(event) {
        this.setState({[event.target.id] : event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        if(this.validateForm()){
        fetch(`http://localhost:8080/user/sign-in`,{
             // Adding method type
    method: "POST",
      
    // Adding body or contents to send
    body: JSON.stringify({
        email:`${this.state.user}`,
        password:`${this.state.password}`
    }),
      
    // Adding headers to the request
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    }
        }).then(response => {
            if (response.status === 200) {
                this.props.authenticate(true);                
                // this.props.history.push('/accounts', {'user': this.state.user});
                this.props.history.push('/overview', {'user': this.state.user});
            } else {
                alert("Invalid user or password!");
                this.setState({user:''});
                this.setState({password:''});
            }
        })
       .catch(err => this.setState({ requestFailed: true }))
    }
    }
    render() {
        return (
            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="user" bsSize="large">
                        <ControlLabel>UserName</ControlLabel>
                        <FormControl
                            autoFocus
                            type="user"
                            placeholder="username"
                            value={this.state.user}
                            onChange={this.handleChange}
                         />
                         <div className="text-danger">{this.state.errors.user}</div>
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            type="password"
                            placeholder="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                    />
                     <div className="text-danger">{this.state.errors.password}</div>
                    </FormGroup>
                    <Button id="submit" block bsSize="large" type="submit">
                        Login
                    </Button>
                </form>
            </div>
        );
    }
}
export default Login;
