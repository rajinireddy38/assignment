package com.bank.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.bank.demo.entities.Transactions;

@Repository
public interface TransactionRepository extends JpaRepository<Transactions, Long> {
    public List<Transactions> findByAccountId(long accountId);    
}
