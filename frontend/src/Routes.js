import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import Login from "./containers/Login";
import Transactions from "./containers/Transactions";
import Accounts from "./containers/Accounts";
import NotFound from "./containers/NotFound";
import Register from "./containers/Register";
import Overview from "./containers/Overview";
import Profile from "./containers/Profile";

export default ({ childProps }) =>
  <Switch>
    <Route path="/" exact component={Home} />;
    <Route path="/login" render={(props) => <Login {...props} {...childProps} />} />;
    <Route path="/overview" render={(props) => <Overview {...props} {...childProps} />} />;
    <Route path="/profile" render={(props) => <Profile {...props} {...childProps} />} />;
    <Route path="/signup" render={(props) => <Register {...props} {...childProps} />} />;
    <Route path="/accounts" render={(props) => <Accounts {...props} {...childProps} />} />;
    <Route path="/transactions" render={(props) => <Transactions {...props} {...childProps} />} />;    
    <Route component={NotFound} />;
  </Switch>;
