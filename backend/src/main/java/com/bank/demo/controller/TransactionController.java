package com.bank.demo.controller;

import java.util.List;
import java.util.Map;

import com.bank.demo.entities.Transactions;
import com.bank.demo.service.TransactionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin
@RestController
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @GetMapping("/transactions")
    public ResponseEntity<List<Transactions>> getTransactionsByAccountId(@RequestParam long accountId) {
        return ResponseEntity.ok(transactionService.getTransactionsByAccountId(accountId));
    }

    @PostMapping("/transactions")
    public ResponseEntity<Transactions> updateTransactionByAccountId(@RequestBody Map<String,String> bodyParams){
        long amount = Long.parseLong(bodyParams.get("amount").toString());
        String type = bodyParams.get("type").toString();
        long accountId = Long.parseLong(bodyParams.get("accId").toString());
        return ResponseEntity.ok(transactionService.createTransactions(amount,type,accountId));
    }

}
