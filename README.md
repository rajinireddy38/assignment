# Overview:
This project is a simple banking application which allows a user to 
- Register themselves to the application.
- Login and view their accounts and transactions.
- Add accounts.
- Deposit/withdraw the amount.

## Components used:	
### Frontend: -
- React.

### Backend: -
- Spring boot.
- H2 Db.
- Java 11.


### Source Code: - 
Source code can be accessed here: [Link](https://gitlab.com/rajinireddy38/assignment). 

## Steps to execute in the local machine:
### Pre-Requisites: -
- [Java](https://www.oracle.com/java/technologies/javase-downloads.html).
- [Maven](https://maven.apache.org/install.html) to build and run the Spring boot app.
- [Node](https://nodejs.org/en/download/) to install the node modules and start the React app.


### Commands to run the frontend react app: -
- Navigate to frontend folder.
- Run `npm install` command.
- Run `npm start` command.

Now the frontend react app will start running on port 3000 and can be accessed using “http://localhost:3000”.

### Commands to run the Spring boot app: -
- Navigate to backend folder.
- Run `mvn clean install` command.
- Navigate to /target folder under /backend.
- Run `java -jar demo-0.0.1-SNAPSHOT.jar` command.

Embedded Tomcat server will start running on port 8080 and front end will invoke the Spring Boot app using port 8080.
During service startup, Tables are created in H2 database and sample entries are inserted.

## Application Walkthrough:
- Access http://localhost:3000 in chrome:
    Sample Test Credentials which can be used - 
    Username:testuser1@nl.com
    Password:test@123

    ![picture](/image/home.png)

- Login button on top right side navigates to login page as shown below:

    ![picture](/image/login.PNG)

- Signup button on top right side navigates to register page for user registration:

    ![picture](/image/register.PNG)

- Once you login an overview page is displayed wherein there is a link to accounts and profile page. 

    ![picture](/image/overview.PNG)

- Upon clicking the profile button, you will be navigated to profile page and by clicking on account button, you will be navigated to accounts page.

- Profile:

    ![picture](/image/profile.PNG)

- Accounts:

    ![picture](/image/accounts.PNG)

- Add Account is for creating new account for the existing user. Initial deposit amount is the initial credit amount while creating the account. 

    ![picture](/image/addAccount.PNG)

- View transactions in the accounts page is used for displaying the transaction details of the account. 

    ![picture](/image/transactions.PNG)

- Add Transaction is for crediting or debiting the amount from the account. 

    ![picture](/image/addTransactions.PNG)

## CICD pipelines and Docker container:
### Prerequisites:
- GitLab account.
- Docker installed in local machine/VM.

### Description:
The project consists of two components – frontend and backend. So, I have used one pipeline for both frontend and backend in GitLab. Pipeline consists of three stages – one for frontend build and publish, other two are for backend maven build and docker build and publish. 

![picture](/image/CIPipeline.PNG)


#### How to run Docker containers on local machine:

- Frontend Image: - Execute the below command to run the image generated in the CI pipeline.

`docker run -it -p 3000:3000 registry.gitlab.com/rajinireddy38/assignment:frontend-master_8b843f21`

Note: 
1. > assignment:frontend-master_8b843f21 – image generated in CI Pipeline.

- Backend Image: - Execute the below command to run the image generated in the CI pipeline.

`docker run -it -p 8080:8080 registry.gitlab.com/rajinireddy38/assignment:backend-master_8b843f21`

Note: 
1. > assignment:backend-master_8b843f21 – image generated in CI Pipeline.







